# convert2iso

Simple and effective program to convert image files to standard ISO image files.

components:

* FRONTEND: easy bash gui: https://github.com/BashGui/easybashgui
    * dialog or kdedialog or zenity or gtkdialog
* BACKEND: cdimage2iso : https://github.com/wdlkmpx/cdimage2iso